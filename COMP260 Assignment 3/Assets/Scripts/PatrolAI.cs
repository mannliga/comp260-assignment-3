﻿using UnityEngine;
using System.Collections;

public class PatrolAI : MonoBehaviour {
	
	public Transform[] points;
	public float patrolSpeed = 3f;

	private int destPoint = 0;
	private NavMeshAgent agent;

	void Start () { 
		agent = GetComponent<NavMeshAgent> ();
		agent.speed = patrolSpeed;
		agent.autoBraking = false;

		GoToNextPoint ();
	}

	void GoToNextPoint() {
		//returns if no patrol points are set up
		if (points.Length == 0)
			return;

		//sets the agent to go to the currently selected destination
		agent.destination = points[destPoint].position;

		//choose the next point in the array as the destination, cycling to start if necessary
		destPoint = (destPoint + 1)%points.Length;
	}

	void Update (){
		//choose the next destination point when the agent gets close to the current
		if (agent.remainingDistance < 0.5f)
			GoToNextPoint ();
	}
}