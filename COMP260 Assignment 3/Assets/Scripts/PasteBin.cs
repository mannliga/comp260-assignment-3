﻿using UnityEngine;
using System.Collections;

public class PasteBin : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public float destroyRadius = 1.0f;
	public LayerMask enemyLayer;

	private bool dead = false;


	// Update is called once per frame
	void Update () {

		if (!dead) 	{

			//get the input vlaues
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			//scale the velocity by the frame duration
			Vector2 velocity = direction * maxSpeed;

			//move the object
			transform.Translate (velocity * Time.deltaTime);
		}
	}

	void OnCollisionEnter (Collision collision) {
		if(enemyLayer.Contains(collision.gameObject)) {
			Debug.Log("Hi");
		}
	}
}
